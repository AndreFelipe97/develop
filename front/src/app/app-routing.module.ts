import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaClientesComponent } from './clientes/lista-clientes/lista-clientes.component';
import { CadastrarClientesComponent } from './clientes/cadastrar-clientes/cadastrar-clientes.component';
import { ListaMedicosComponent } from './medicos/lista-medicos/lista-medicos.component';
import { FormMedicosComponent } from './medicos/form-medicos/form-medicos.component';
import { CadastrarMedicoComponent } from './medicos/cadastrar-medico/cadastrar-medico.component';
import { ListaConsultasComponent } from './consultas/lista-consultas/lista-consultas.component';
import { CadastrarConsultasComponent } from './consultas/cadastrar-consultas/cadastrar-consultas.component';


const routes: Routes = [
  // Rotas dos pacientes
  {path: 'lista/clientes', component: ListaClientesComponent},
  {path: 'formulario/clientes', component: CadastrarClientesComponent},
  /*{path: 'cliente/editar/:id', component: EditarClientesComponent, canActivate: [AuthGuard]},*/

  // Rotas do medicos
  {path: 'lista/medicos', component: ListaMedicosComponent},
  {path: 'formulario/medicos', component: CadastrarMedicoComponent},

  // Rotas consultas
  {path: 'lista/consultas', component: ListaConsultasComponent},
  {path: 'formulario/consultas', component: CadastrarConsultasComponent},

  // Redirecionamento pra home
  {path: '**', redirectTo: 'lista/clientes'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
