import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Medicos } from '../medicos';

@Component({
  selector: 'app-form-medicos',
  templateUrl: './form-medicos.component.html',
  styleUrls: ['./form-medicos.component.css']
})
export class FormMedicosComponent {
  
  @Input() medico: Medicos = <Medicos> {};
  @Output() outputMedico: EventEmitter<Medicos> = new EventEmitter();

  onSubmit() {
    this.outputMedico.emit(this.medico);
  }

  constructor() { }
}
