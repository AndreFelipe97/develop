import { Component, OnInit, Output } from '@angular/core';
import { Medicos } from '../medicos';
import { MedicosService } from '../medicos.service';

@Component({
  selector: 'app-lista-medicos',
  templateUrl: './lista-medicos.component.html',
  styleUrls: ['./lista-medicos.component.css']
})
export class ListaMedicosComponent implements OnInit {

  constructor( private medicoService: MedicosService ) { }

  @Output() filter ="";
  public medicos: Medicos[];
  public paginaAtual = 1; // Dizemos que queremos que o componente quando carregar, inicialize na página 1.

  ngOnInit() {
    this.getListaMedicos();
  }

  getListaMedicos() {
    this.medicoService.getListaMedicos()
      .subscribe((medicos: Medicos[]) => {
        this.medicos = medicos;
      });
  }
  deletaMedicos(id: number) {
    this.medicoService.deletaMedicos(id)
      .subscribe(() => {
        this.getListaMedicos();
      });
  }
  existemClientes(): boolean {
    return this.medicos && this.medicos.length > 0;
  }
  key: string = 'nome'; // Define um valor padrão, para quando inicializar o componente
    reverse: boolean = false;
    sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
    }


}
