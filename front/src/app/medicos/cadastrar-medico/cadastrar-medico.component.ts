import { Component } from '@angular/core';
import { MedicosService } from '../medicos.service';
import { Router } from '@angular/router';
import { Medicos } from '../medicos';

@Component({
  selector: 'app-cadastrar-medico',
  templateUrl: './cadastrar-medico.component.html',
  styleUrls: ['./cadastrar-medico.component.css']
})
export class CadastrarMedicoComponent {

  constructor(private medicosServices: MedicosService, private router: Router) { }

  addMedico(medico: Medicos) {
    this.medicosServices.addMedico(medico)
      .subscribe (
        () => {this.router.navigateByUrl('/lista/medicos'); }
      );
  }

}
