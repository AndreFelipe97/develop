import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Medicos } from './medicos';


@Injectable({
  providedIn: 'root'
})
export class MedicosService {

  constructor(private http: HttpClient) { }

  getListaMedicos(): Observable<Medicos[]> {
    const url = `${environment.medicosApiUrl}/medicos`;
    return this.http.get<Medicos[]>(url);
  }
  getMedico(id: number): Observable<Medicos> {
    const url = `${environment.medicosApiUrl}/medicos/${id}`;
    return this.http.get<Medicos>(url);
  }
  addMedico(medicos: Medicos): Observable<Medicos> {
    const url = `${environment.medicosApiUrl}/medicos/`;
    return this.http.post<Medicos>(url, medicos);
  }
  atualiazarMedicos(medicos: Medicos): Observable<Medicos> {
    const url = `${environment.medicosApiUrl}/medicos/${medicos.id}`;
    return this.http.put<Medicos>(url, medicos);
  }
  deletaMedicos(id: number): Observable<Medicos> {
    const url = `${environment.medicosApiUrl}/medicos/${id}`;
    return this.http.delete<Medicos>(url);
  }

}
