export interface Medicos {
    id: number;
    nome: string;
    telefone: string;
    email: string;
    crm: string;
}
