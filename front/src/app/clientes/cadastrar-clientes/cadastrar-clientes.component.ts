import { Component } from '@angular/core';
import { ClientesService } from '../clientes.service';
import { Router } from '@angular/router';
import { Clientes } from '../clientes';

@Component({
  selector: 'app-cadastrar-clientes',
  templateUrl: './cadastrar-clientes.component.html',
  styleUrls: ['./cadastrar-clientes.component.css']
})
export class CadastrarClientesComponent {

  constructor(private clientesServices: ClientesService, private router: Router) { }

  addCliente(cliente: Clientes) {
    this.clientesServices.addCliente(cliente)
      .subscribe (
        () => {this.router.navigateByUrl('/lista/clientes'); }
      );
  }

}
