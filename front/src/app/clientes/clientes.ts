export interface Clientes {
    id: number;
    nome: string;
    telefone: string;
    sexo: string;
    endereco: string;
}
