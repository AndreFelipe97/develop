import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Clientes } from './clientes';


@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  constructor(private http: HttpClient) { }

  getListaClientes(): Observable<Clientes[]> {
    const url = `${environment.medicosApiUrl}/pacientes`;
    return this.http.get<Clientes[]>(url);
  }
  getCliente(id: number): Observable<Clientes> {
    const url = `${environment.medicosApiUrl}/pacientes/${id}`;
    return this.http.get<Clientes>(url);
  }
  addCliente(clientes: Clientes): Observable<Clientes> {
    const url = `${environment.medicosApiUrl}/pacientes/`;
    return this.http.post<Clientes>(url, clientes);
  }
  atualiazarClientes(clientes: Clientes): Observable<Clientes> {
    const url = `${environment.medicosApiUrl}/pacientes/${clientes.id}`;
    return this.http.put<Clientes>(url, clientes);
  }
  deletaClientes(id: number): Observable<Clientes> {
    const url = `${environment.medicosApiUrl}/pacientes/${id}`;
    return this.http.delete<Clientes>(url);
  }
}
