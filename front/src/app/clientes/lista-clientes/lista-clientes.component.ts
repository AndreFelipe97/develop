import { Component, OnInit, Output } from '@angular/core';
import { Clientes } from '../clientes';
import { ClientesService } from '../clientes.service';

@Component({
  selector: 'app-lista-clientes',
  templateUrl: './lista-clientes.component.html',
  styleUrls: ['./lista-clientes.component.css']
})
export class ListaClientesComponent implements OnInit {

  constructor( private clienteService: ClientesService ) { }

  @Output() filter ="";
  public clientes: Clientes[];
  public paginaAtual = 1; // Dizemos que queremos que o componente quando carregar, inicialize na página 1.
  
  ngOnInit() {
    this.getListaClientes();
  }

  getListaClientes() {
    this.clienteService.getListaClientes()
      .subscribe((clientes: Clientes[]) => {
        this.clientes = clientes;
      });
  }
  deletaCliente(id: number) {
    this.clienteService.deletaClientes(id)
      .subscribe(() => {
        this.getListaClientes();
      });
  }
  existemClientes(): boolean {
    return this.clientes && this.clientes.length > 0;
  }
  key: string = 'nome'; // Define um valor padrão, para quando inicializar o componente
    reverse: boolean = false;
    sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
    }

}
