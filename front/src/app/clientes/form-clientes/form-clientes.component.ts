import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Clientes } from '../clientes';

@Component({
  selector: 'app-form-clientes',
  templateUrl: './form-clientes.component.html',
  styleUrls: ['./form-clientes.component.css']
})
export class FormClientesComponent {

  @Input() cliente: Clientes = <Clientes> {};
  @Output() outputCliente: EventEmitter<Clientes> = new EventEmitter();

  onSubmit() {
    this.outputCliente.emit(this.cliente);
  }


  constructor() { }

}
