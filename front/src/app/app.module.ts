import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter'; // Importação
import { OrderModule } from 'ngx-order-pipe';  // Módulo da dependência de paginação
import { NgxPaginationModule } from 'ngx-pagination'; // Módulo da dependência de paginação


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListaClientesComponent } from './clientes/lista-clientes/lista-clientes.component';
import { MenuComponent } from './menu/menu.component';
import { FormClientesComponent } from './clientes/form-clientes/form-clientes.component';
import { CadastrarClientesComponent } from './clientes/cadastrar-clientes/cadastrar-clientes.component';
import { ListaMedicosComponent } from './medicos/lista-medicos/lista-medicos.component';
import { FormMedicosComponent } from './medicos/form-medicos/form-medicos.component';
import { CadastrarMedicoComponent } from './medicos/cadastrar-medico/cadastrar-medico.component';
import { ListaConsultasComponent } from './consultas/lista-consultas/lista-consultas.component';
import { CadastrarConsultasComponent } from './consultas/cadastrar-consultas/cadastrar-consultas.component';
import { FormConsultasComponent } from './consultas/form-consultas/form-consultas.component';

@NgModule({
  declarations: [
    AppComponent,
    ListaClientesComponent,
    MenuComponent,
    FormClientesComponent,
    CadastrarClientesComponent,
    ListaMedicosComponent,
    FormMedicosComponent,
    CadastrarMedicoComponent,
    ListaConsultasComponent,
    CadastrarConsultasComponent,
    FormConsultasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    Ng2SearchPipeModule, // Nosso módulo recém instalado
    OrderModule, // Nosso módulo recém instalado
    NgxPaginationModule // Nosso módulo recém instalado
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
