import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Consultas } from './consultas';

@Injectable({
  providedIn: 'root'
})
export class ConsultasService {

  constructor(private http: HttpClient) { }

  getListaConsultas(): Observable<Consultas[]> {
    const url = `${environment.medicosApiUrl}/consultas`;
    return this.http.get<Consultas[]>(url);
  }
  getConsulta(id: number): Observable<Consultas> {
    const url = `${environment.medicosApiUrl}/consultas/${id}`;
    return this.http.get<Consultas>(url);
  }
  addConsulta(consultas: Consultas): Observable<Consultas> {
    const url = `${environment.medicosApiUrl}/consultas/`;
    return this.http.post<Consultas>(url, consultas);
  }
  atualiazarConsultas(consultas: Consultas): Observable<Consultas> {
    const url = `${environment.medicosApiUrl}/consultas/${consultas.id}`;
    return this.http.put<Consultas>(url, consultas);
  }
  deletaConsultas(id: number): Observable<Consultas> {
    const url = `${environment.medicosApiUrl}/consultas/${id}`;
    return this.http.delete<Consultas>(url);
  }
}
