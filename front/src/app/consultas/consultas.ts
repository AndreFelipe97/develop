export interface Consultas {
    id: number;
    pacienteID: number;
    medicoID: number;
    data: Date;
}
