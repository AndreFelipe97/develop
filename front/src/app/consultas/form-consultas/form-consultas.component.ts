import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Consultas } from '../consultas';

@Component({
  selector: 'app-form-consultas',
  templateUrl: './form-consultas.component.html',
  styleUrls: ['./form-consultas.component.css']
})
export class FormConsultasComponent {
  @Input() consultas: Consultas = <Consultas> {};
  @Output() outputConsulta: EventEmitter<Consultas> = new EventEmitter();

  onSubmit() {
    this.outputConsulta.emit(this.consultas);
  }


  constructor() { }
}
