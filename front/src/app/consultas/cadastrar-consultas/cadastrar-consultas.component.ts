import { Component } from '@angular/core';
import { ConsultasService } from '../consultas.service';
import { Router } from '@angular/router';
import { Consultas } from '../consultas';

@Component({
  selector: 'app-cadastrar-consultas',
  templateUrl: './cadastrar-consultas.component.html',
  styleUrls: ['./cadastrar-consultas.component.css']
})
export class CadastrarConsultasComponent {

  constructor(private consultasServices: ConsultasService, private router: Router) { }

  addConsultas(consulta: Consultas) {
    this.consultasServices.addConsulta(consulta)
      .subscribe (
        () => {this.router.navigateByUrl('/lista/consultas'); }
      );
  }

}
