import { Component, OnInit, Output } from '@angular/core';
import { Consultas } from '../consultas';
import { ConsultasService } from '../consultas.service';

@Component({
  selector: 'app-lista-consultas',
  templateUrl: './lista-consultas.component.html',
  styleUrls: ['./lista-consultas.component.css']
})
export class ListaConsultasComponent implements OnInit {

  constructor( private consultasService: ConsultasService ) { }

  @Output() filter ="";
  public consultas: Consultas[];
  public paginaAtual = 1; // Dizemos que queremos que o componente quando carregar, inicialize na página 1.
  
  ngOnInit() {
    this.getListaConsultas();
  }

  getListaConsultas() {
    this.consultasService.getListaConsultas()
      .subscribe((consulta: Consultas[]) => {
        this.consultas = consulta;
      });
  }
  deletaCliente(id: number) {
    this.consultasService.deletaConsultas(id)
      .subscribe(() => {
        this.getListaConsultas();
      });
  }
  existemClientes(): boolean {
    return this.consultas && this.consultas.length > 0;
  }
  key: string = 'nome'; // Define um valor padrão, para quando inicializar o componente
    reverse: boolean = false;
    sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
    }

}
