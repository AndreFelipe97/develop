﻿using ClinicaSIM.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClinicaSIM.Database
{
    public class ClinicaSIMContext: DbContext
    {
        public ClinicaSIMContext(DbContextOptions<ClinicaSIMContext> options) : base(options)
        {

        }

        public DbSet<Medico> Medicos { get; set; }
        public DbSet<Paciente> Pacientes { get; set; }
        public DbSet<Consulta> Consultas { get; set; }

    }
}
