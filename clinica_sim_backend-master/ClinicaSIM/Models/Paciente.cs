﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ClinicaSIM.Models
{
    public class Paciente
    {
        [Key]
        public int PacienteID { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Telefone { get; set; }
        [Required]
        public string Sexo { get; set; }
        [Required]
        public string Endereco { get; set; }

    }
}
