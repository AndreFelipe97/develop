﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ClinicaSIM.Models
{
    public class Consulta
    {
        [Key]
        public int ConsultaID { get; set; }
        [Required]
        public int PacienteID { get; set; }

        [Required]
        public int MedicoID { get; set; }

        [Required]
        public DateTime Data{ get; set; }
    }
}
